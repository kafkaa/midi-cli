"""midi 2.0 [2022-07-01]

COMMAND:
    sysex

USAGE:
    midi sysex send : load a sysex file and send through the active out port
    midi sysex help : display comprehensive documentation

for more info, see midi specs @https://www.midi.org/specifications
"""
import sys
import rtmidi
from time import sleep
from src.utils import setopts
from src.utils import fileops
from src.utils.sendsyx import send_system_exclusive

this = sys.modules[__name__]


class sysex(setopts.SetOpts):
    """midi 2.0 [2022-07-01]

NOTE:
    SEND:
        SEND SYSTEM EXCLUSIVE MESSAGE

        the 'sysex send' command is used to send system exclusive data to the active
        midi out port. It loads and sends the .syx file specified in the command line
        argument.

        ARGUMENTS:
            -f: path to the .syx file

        USAGE:
            midi sysex send -f /path/to/file.syx

    HELP:
        SHOWS COMMAND DOCUMENTATION

        USAGE: midi sysex help

for more info, see midi specs @https://www.midi.org/specifications
"""
    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        match self.options.cmd:
            case 'send':
                send_system_exclusive(fileops.read(fileops.MOUT), self.options.f)

            case 'recv':
                sys.exit('RECV not implemented')
