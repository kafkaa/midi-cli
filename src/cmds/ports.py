"""midi 2.0 [2022-07-01]

COMMAND:
    midi ports

USAGE:
    midi ports available            shows available midi ports
    midi ports input $PORT_NUMBER   set the active midi input port
    midi ports output $PORT_NUMBER  set the active midi output port
    midi ports HELP                 display comprehensive documentation

for more info, see midi specs @https://www.midi.org/specifications
"""
import sys
import platform
import rtmidi
from rtmidi import midiutil
from src.utils import setopts
from src.utils import fileops

this = sys.modules[__name__]


class ports(setopts.SetOpts):
    """midi 2.0 [2022-07-01]

PORTS
    AVAILABLE:
        GET A LIST MIDI PORTS AVAILABLE ON THE SYSTEM

        the 'ports available' command is used to display the midi ports
        currently available on the system.

        USAGE: midi ports available

            PORTS AVAILABLE:
                0: IAC Driver Bus 1
                1: IAC Driver Bus 2

    INPUT:
        ACTIVATE A MIDI INPUT PORT

        the 'ports input' command is used to activate one of the available midi
        input ports. It takes an integer as an argument, representing the port
        number to be activated.

        USAGE: midi ports input 0
            --this would activate IAC Driver BUS 1

    OUTPUT:
        ACTIVATE A MIDI OUTPUT PORT

        the 'ports output' command is used to activate one of the available midi
        output ports. It takes an integer as an argument, representing the port
        number to be activated.

        USAGE: midi ports output 0
            --this would activate IAC Driver BUS 1

    HELP:
        SHOWS COMMAND DOCUMENTATION

        USAGE: midi ports help

MIDI PORTS

Midi ports determine how MIDI events are directed to one or several virtual
and/or hardware devices, such as a DAWs or synthesizers. MIDI has an IN, OUT and
Thru ports and these connectors are each a unique path having 16 different
channels that are used to pass note, control and program data.

for more info, see midi specs @https://www.midi.org/specifications
"""
    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        match self.options.cmd:
            case 'input':
                port = self.options.inport
                fileops.write(port, fileops.MIN)
                print(f'active input port set to {port}')
                sys.exit(0)

            case 'output':
                port = self.options.outport
                fileops.write(port, fileops.MOUT)
                print(f'active output port set to {port}')
                sys.exit(0)

            case 'available':
                print("checking system for available midi ports...")

                midiutil.list_output_ports()
                midiutil.list_input_ports()

            case 'active':
                print("checking active midi ports...")
                midiout, midiin = rtmidi.MidiOut(), rtmidi.MidiIn()
                outputs, inputs = midiout.get_ports(), midiin.get_ports()

                try:
                    print(f'ACTIVE INPUT:  listening to {inputs[int(fileops.read(fileops.MIN))]}')

                except IndexError:
                    fileops.write('0', fileops.MIN)
                    print(f'ACTIVE INPUT:  listening to {inputs[0]}')

                try:
                    print(f'ACTIVE OUTPUT: sending to   {outputs[int(fileops.read(fileops.MOUT))]}')

                except IndexError:
                    fileops.write('0', fileops.MOUT)
                    print(f'ACTIVE OUTPUT: sending to   {outputs[0]}')

                del midiout
                del midiin
