"""midi 2.0 [2022-07-01]

COMMAND:
    control

USAGE:
    midi control send       send a midi control change message
    midi control help       display comprehensive documentation

for more info, see midi specs @https://www.midi.org/specifications
"""
import sys
import rtmidi
from time import sleep
from src.utils import setopts
from src.utils import fileops
from src.utils.events import ControlChange

this = sys.modules[__name__]


class control(setopts.SetOpts):
    """midi 2.0 [2022-07-01]

CONTROL:
    SEND:
        SEND A MIDI CONTROL CHANGE MESSAGE

        the 'control' command is used to send midi control change data
        to the active midi-out port.

        ARGUMENTS:
            -p: paramter number 0-127
            -v: the parameter value 0-127
            -c: midi channel 1-16 (optional, defaults to 10)

        USAGE: midi control send -p 5 -v 100 -c 16

    HELP:
        SHOWS COMMAND DOCUMENTATION

        USAGE: midi control help

CONTROLLER

Sets a particular controller's value. A controller is any switch, slider,
knob, etc, that implements some function (usually) other than sounding or
stopping notes (ie, which are the jobs of the Note On and Note Off messages
respectively). There are 128 possible controllers on a MIDI device. These
are numbered from 0 to 127. Some of these controller numbers are assigned
to particular hardware controls on a MIDI device. For example, controller 1
is the Modulation Wheel. Other controller numbers are free to be arbitrarily
interpreted by a MIDI device. For example, a drum box may have a slider
controlling Tempo which it arbitrarily assigns to one of these free numbers.
Then, when the drum box receives a Controller message with that controller
number, it can adjust its tempo. A MIDI device need not have an actual
physical control on it in order to respond to a particular controller.
For example, even though a rack-mount sound module may not have a Mod Wheel
on it, the module will likely still respond to and utilize Modulation c
ontroller messages to modify its sound. If the device is a MultiTimbral
unit, then each one of its Parts may respond differently (or not at all)
to various controller numbers. The Part affected by a particular controller
message is the one assigned to the message's MIDI channel.

STATUS BYTE

0xB0 to 0xBF where the low nibble is the MIDI channel.

DATA BYTES

Two data bytes follow the Status Byte.

The first data is the controller number (0 to 127). This indicates which
controller is affected by the received MIDI message.

The second data byte is the value to which the controller should be set,
a value from 0 to 127.

ERRATA

An All Controllers Off controller message can be used to reset all controllers
(that a MIDI device implements) to default values. For example, the Mod Wheel
is reset to its "off" position upon receipt of this message.

See the list of Defined Controller Numbers for more information about particular
controllers @http://midi.teragonaudio.com/tech/midispec/ctllist.htm

for more info, see midi specs @https://www.midi.org/specifications
"""

    def execute(self):
        """sends a single midi control change event"""
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        def setchannel(x):
            return 10 if x is None else x

        value = int(self.options.v)
        control = int(self.options.p)
        channel = int(setchannel(self.options.c))
        interface = fileops.read(fileops.getport)

        MIDIOUT = rtmidi.MidiOut()
        _ = MIDIOUT.open_port(int(interface))

        with MIDIOUT:
            event = ControlChange(parameter=control, channel=channel)
            MIDIOUT.send_message(event(value))
            sleep(0.1)

        MIDIOUT.close_port()
