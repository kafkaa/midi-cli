"""midi 2.0 [2022-07-01]

COMMAND:
    midi program

USAGE:
    midi program send       send a midi program message
    midi program help       display comprehensive documentation

for more info, see midi specs @https://www.midi.org/specifications
"""
import sys
import rtmidi
from time import sleep
from src.utils import setopts
from src.utils import fileops
from src.utils.events import ProgramChange

this = sys.modules[__name__]


class program(setopts.SetOpts):
    """midi 2.0 [2022-07-01]

PROGRAM:
    SEND:
        SEND A MIDI PROGRAM CHANGE MESSAGE

        the 'program send' command is used to send a midi program change 
        to the active midi-out port. 

        ARGUMENTS: 
            -v : the patch value 0-127
            -c : midi channel 1-16 (optional, defaults to 10)

        USAGE: midi program send -v 100 -c 16

    HELP:
        SHOWS COMMAND DOCUMENTATION

        USAGE: midi program help


MIDI PROGRAM CHANGE:

Causes the MIDI device to change to a particular Program (which some 
devices refer to as Patch, or Instrument, or Preset). Most sound modules 
have a variety of instrumental sounds, such as Piano, and Guitar, and Trumpet, 
and Flute, etc. Each one of these instruments is contained in a Program. 
So, changing the Program changes the instrumental sound that the MIDI 
device uses when it plays Note On messages. 

Of course, other MIDI messages also may modify the current Program's 
sound. But, the Program Change message actually selects which instrument 
currently plays. There are 128 possible program numbers, from 0 to 127. 
If the device is a MultiTimbral unit, then it usually can play 16 "Parts" 
at once, each receiving data upon its own MIDI channel. This message will 
then change the instrument sound for only that Part which is set to the 
message's MIDI channel.

For MIDI devices that don't have instrument sounds, such as a Reverb unit 
which may have several Preset "room algorithms" stored, the Program Change 
message is often used to select which Preset to use. As another example, 
a drum box may use Program Change to select a particular rhythm pattern 
(ie, drum beat).

STATUS BYTE

0xC0 to 0xCF where the low nibble is the MIDI channel.

DATA BYTES

One data byte follows the status. It is the program number to change to, 
a number from 0 to 127.

ERRATA

On MIDI sound modules (ie, whose Programs are instrumental sounds), it became 
desirable to define a standard set of Programs in order to make sound modules 
more compatible. This specification is called General MIDI Standard.

Just like with MIDI channels 0 to 15 being displayed to a musician as 
channels 1 to 16, many MIDI devices display their Program numbers starting 
from 1 (even though a Program number of 0 in a Program Change message selects 
the first program in the device). On the other hand, this approach was never 
standardized, and some devices use vastly different schemes for the musician 
to select a Program. For example, some devices require the musician to specify 
a bank of Programs, and then select one within the bank (with each bank typically 
containing 8 to 10 Programs). So, the musician might specify the first Program 
as being bank 1, number 1. Nevertheless, a Program Change of number 0 would 
select that first Program.

Receipt of a Program Change should not cut off any notes that were previously 
triggered on the channel, and which are still sustaining.

for more info, see midi specs @https://www.midi.org/specifications
"""

    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        def setchannel(x):
            return 10 if x is None else x

        program = int(self.options.v)
        channel = int(setchannel(self.options.c))
        interface = fileops.read(fileops.getport)

        MIDIOUT = rtmidi.MidiOut()
        _ = MIDIOUT.open_port(int(interface))

        with MIDIOUT:
            event = ProgramChange(channel=channel)
            MIDIOUT.send_message(event(program))
            sleep(0.1)

        MIDIOUT.close_port()
