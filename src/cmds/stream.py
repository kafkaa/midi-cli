"""midi 2.0 [2022-07-01]

COMMAND:
    stream

USAGE:
    midi stream input : listen to the input of the active port in realtime
    midi stream help  : display comprehensive documentation

for more info, see midi specs @https://www.midi.org/specifications
"""
import sys
import rtmidi
from time import sleep
from src.utils import setopts
from src.utils import fileops
from src.utils.stream import MidiStreamer

this = sys.modules[__name__]

class stream(setopts.SetOpts):
    """midi 2.0 [2022-07-01]

STREAM:
    INPUT:
        LISTEN TO AND DISPLAY THE INPUT STREAM OF THE ACTIVE PORT

        the monitor stream command opens the input of the active midi port and
        passes the incoming midi data to the stdout in realtime.

        ARGUMENTS:
        there are two optional flags:
            -c for filtering midi clock messages out of the feed
            -s for filtering system exclusive messages out of the feed.

        USAGE:
            midi stream input -cs

    HELP:
        SHOWS COMMAND DOCUMENTATION

        USAGE: midi stream help

for more info, see midi specs @https://www.midi.org/specifications
"""
    def output(self, x, y, z):
        sys.stdout.write("%s-%s-%s\n" % (x, y, z))
        sys.stdout.flush()

    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        clk, syx = self.options.c, self.options.s

        midi_input = rtmidi.MidiIn()

        streamer = MidiStreamer(midi_input, clock=clk, sysex=syx, callback=self.output)
        streamer.open(int(fileops.read(fileops.MIN)))
        streamer.listen()

        sleep(0.5)
        del midi_input
        sys.exit()
