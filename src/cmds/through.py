"""midi 2.0 [2022-07-01]

COMMAND:
    through

USAGE:
    midi through display    : print formatted message from stdin to the stdout
    midi through lcd        : format and sent midi stream data to a serial lcd
    midi through help       : display comprehensive documentation

more info:
see midi specs @https://www.midi.org/specifications
"""
import sys
from src.utils import setopts
from src.utils import faderfox
from src.utils import functions

this = sys.modules[__name__]

class through(setopts.SetOpts):
    """midi 2.0 [2022-07-01]

THROUGH:
   DISPLAY
        FORMAT THE STANDARD INPUT AND PRINT TO THE STANDARD OUTPUT

        the through display command formats and prints the incoming message data
        to the stdout in realtime. It must be used to receive data from a
        MidiStream pipe.

        ARGUMENTS:
            OPTIONAL:
            -d prints delta time
            -t prints the event timestamp

        USAGE:
            midi stream input | midi through display -dt

    LCD:
        DISPLAYS MIDI DATA ON AN LCD DISPLAY DEVICE
        
        ARGUMENTS:
            MANDATORY:
            device the path to the serial device (/dev/...)

            OPTIONAL:
            -s sets the splash screen of the LCD to display at startup

        USAGE:
            midi stream input | midi through lcd /dev/ttyACM0 -s Faderfox PC4

    HELP:
        SHOWS COMMAND DOCUMENTATION

        USAGE: midi stream help

for more info:
see midi specs @https://www.midi.org/specifications
"""
    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        match self.options.cmd:
            case 'display':
                functions.display()

            case 'lcd':
                dev = self.options.device
                screen = self.options.s
                functions.lcd_display(dev, splash=screen, data=faderfox.PRESETS, format=faderfox.fmsg)

            case _:
                sys.exit("invalid command")

