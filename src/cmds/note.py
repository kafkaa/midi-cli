"""midi 2.0 [2022-07-01]

COMMAND:
    note

USAGE:
    midi note send       send a midi note message to the active port
    midi note help       display comprehensive documentation

for more info, see midi specs @https://www.midi.org/specifications
"""
import sys
import rtmidi
from time import sleep
from src.utils import setopts
from src.utils import fileops
from src.utils.events import MidiNote

this = sys.modules[__name__]


class note(setopts.SetOpts):
    """midi 2.0 [2022-07-01]

NOTE:
    SEND:
        SEND MIDI NOTE ON/OFF

        the 'note' command is used to send midi note data to the active
        midi out port.

        ARGUMENTS:
            -p: pitch 0-127
            -v: velocity 0-127
            -d: duration (ms)
            -c: midi channel 1-16 (optional, defaults to 10)

        USAGE: midi note send -p 60 -v 100 -d 3600 -c 16

    HELP:
        SHOWS COMMAND DOCUMENTATION

        USAGE: midi control help


NOTE ON/OFF

Indicates that a particular note should be played. Essentially, this means
that the note starts sounding, but some patches might have a long VCA attack
time that needs to slowly fade the sound in. In any case, this message indicates
that a particular note should start playing (unless the velocity is 0, in
which case, you really have a Note Off). If the device is a MultiTimbral unit,
then each one of its Parts may sound Note Ons on its own channel. The Part that
sounds a particular Note On message is the one assigned to the message's MIDI
channel.

STATUS BYTES

0x90 to 0x9F where the low nibble is the MIDI channel.

DATA BYTES

Two data bytes follow the Status.

The first data is the note number. There are 128 possible notes on a MIDI device,
numbered 0 to 127 (where Middle C is note number 60). This indicates which note
should be played.

The second data byte is the velocity, a value from 0 to 127. This indicates
how much force the note should be played (where 127 is the most force). It's
up to a MIDI device how it uses velocity information. Often velocity is be used
to tailor the VCA attack time and/or attack level (and therefore the overall
volume of the note). MIDI devices that can generate Note On messages, but don't
implement velocity features, will transmit Note On messages with a preset velocity
of 64.

A Note On message that has a velocity of 0 is considered to actually be a Note Off
message, and the respective note is therefore released. See the Note Off entry for
a description of such. This "trick" was created in order to take advantage of
running status.

A device that recognizes MIDI Note On messages must be able to recognize both a
real Note Off as well as a Note On with 0 velocity (as a Note Off). There are
many devices that generate real Note Offs, and many other devices that use Note On
with 0 velocity as a substitute.

ERRATA

In theory, every Note On should eventually be followed by a respective Note Off
message (ie, when it's time to stop the note from sounding). Even if the note's
sound fades out (due to some VCA envelope decay) before a Note Off for this note
is received, at some later point a Note Off should be received. For example, if
a MIDI device receives the following Note On:

0x90 0x3C 0x40   Note On/chan 0, Middle C, velocity could be anything except 0

Then, a respective Note Off should subsequently be received at some time, as so:

0x80 0x3C 0x40   Note Off/chan 0, Middle C, velocity could be anything

Instead of the above Note Off, a Note On with 0 velocity could be substituted as so:

0x90 0x3C 0x00   Really a Note Off/chan 0, Middle C, velocity must be 0

If a device receives a Note On for a note (number) that is already playing (ie,
hasn't been turned off yet), it the device's decision whether to layer another
"voice" playing the same pitch, or cut off the voice playing the preceding note
of that same pitch in order to "retrigger" that note.

for more info, see midi specs @https://www.midi.org/specifications
"""

    def execute(self):
        self.validate(this.__doc__)
        self.docs(self.__doc__)

        def setchannel(x):
            return 10 if x is None else x

        pitch = int(self.options.p)
        velocity = int(self.options.v)
        duration = int(self.options.d)
        channel = int(setchannel(self.options.c))
        interface = fileops.read(fileops.getport)

        MIDIOUT = rtmidi.MidiOut()
        _ = MIDIOUT.open_port(int(interface))

        with MIDIOUT:
            event = MidiNote(channel=channel)
            MIDIOUT.send_message(event(pitch, velocity))
            sleep(duration / 1000)
            MIDIOUT.send_message(event(pitch, 0))
            sleep(0.1)

        MIDIOUT.close_port()
