# basic adafruit LCD commands
# see https://learn.adafruit.com/usb-plus-serial-backpack/command-reference
import serial

def connect(device: str) -> serial.Serial:
    s = serial.Serial(device, 57600, timeout=1)
    matrix_write(s, [0x58])
    return s

def disconnect(s: serial.Serial) -> None:
    matrix_write(s, [0x58])
    s.close()

def matrix_write(s: serial.Serial, command: list[int]) -> None:
    command.insert(0, 0xFE)
    s.write(bytearray(command))

def on(s: serial.Serial) -> None:
    matrix_write(s, [0x42, 0])

def off(s: serial.Serial) -> None:
    matrix_write(s, [0x46])

def size(s: serial.Serial, cols: int, rows: int) -> None:
    # set LCD matrix size
    # 16 x 2 = cols: 16, rows: 2
    matrix_write(s, [0xD1, cols, rows])

def clear(s: serial.Serial) -> None:
    # clears the LCD Text
    matrix_write(s, [0x58])

def contrast(s: serial.Serial, value: int) -> None:
    # set contrast value; range is 0-255
    # around 180-220 value is what works best. Setting is saved to EEPROM
    matrix_write(s, [0x50, value])

def brightness(s: serial.Serial, value: int) -> None:
    # set brightness value; range 0-255
    # takes effect after color is color component is set. Saved to EEPROM
    matrix_write(s, [0x99, value])

def color(s: serial.Serial, r: int=255, g: int=255, b: int=255) -> None:
    # r, g, b values range 0-255
    matrix_write(s, [0xD0, r, g, b])

def update(s: serial.Serial, text: str) -> None:
    # clear the display and add new text
    matrix_write(s, [0x58])
    s.write(text.encode())

def autoscroll(s: serial.Serial, on=False) -> None:
    trig = 0x52 if on == False else 0x51
    matrix_write(s, [trig])

def set_splash(s: serial.Serial, text: str) -> None:
    splash = [ord(char) for char in text]
    splash.insert(0, 0x40)
    matrix_write(s, splash)

def set_cursor(s: serial.Serial, x: int, y: int) -> None:
    matrix_write(s, [0x47, x, y])
