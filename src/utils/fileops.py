"""Persistence: File Operations"""
import os
import sys
import json
import sqlite3
from pathlib import Path
from dbs.sql.sqldb import collect_tables

HOME = sys.path[0]

PERSISTENCE = Path(f"{HOME}/src/.persistence")
LCD = Path(os.environ.get("FOXLCDDB"))

MIN = PERSISTENCE / 'input.port'
MOUT = PERSISTENCE / 'output.port'
DBSE = LCD / 'persistence' / 'presets.db'

def write(data, filename):
    with open(filename, 'w') as file:
        file.write(data)

def read(filename):
    with open(filename) as file:
        return file.read()

def load(path: str) -> dict:
    with open(path, 'r') as file:
        return json.load(file)


