from src.utils.status import MODES, VOICE
from src.utils.helpers import attributes, CHMAP

class MidiDataError(Exception):
    pass

class MidiMsg:
    # represents a raw (non system-exclusive) midi byte sequence
    msg = None

    def __init__(self, status: int, *data: int):
        if len(data) > 3:
            raise MidiDataError('midi message has a maximum of 3 bytes')

        if not 127 < status < 256:
            raise MidiDataError('status byte must be within range 128-255')

        if not all(-1 < value < 128 for value in data):
            raise MidiDataError('data bytes must be within range 0-127')

        MidiMsg.msg = status, *data


@attributes(VOICE)
class MidiEvent:
    # base class for midi event types
    def __new__(cls, *args, **kwargs):
        if cls is MidiEvent:
            raise TypeError('base class cannot be instantiated')
        return object.__new__(cls)

    def __init__(self, **kwargs) -> None:
        # kwargs: channel: int=None
        self.__dict__.update(kwargs)

    def set_status(self, status) -> int:
        return int(f"{status}{CHMAP[self.channel]}", 16)

    def set_channel_mode(self, mode: str) -> MidiMsg.msg:
        # modes: omni_on, omni_off, poly_mode, mono_mode, all_notes_off, reset_controllers
        byte = self.control_change
        m = MidiMsg(self.set_status(byte), *MODES[mode])
        return m.msg


class MidiNote(MidiEvent):
    # kwargs: channel: int=None
    def __call__(self, pitch, velocity) -> MidiMsg.msg:
        byte = self.note_on if velocity else self.note_off
        m = MidiMsg(self.set_status(byte), pitch, velocity)
        return m.msg

    def __repr__(self):
        return f"<{type(self).__name__}(Channel {self.channel})>"


class ControlChange(MidiEvent):
    # kwargs: parameter: int=parameter, channel: int=channel
    def __call__(self, value) -> MidiMsg.msg:
        byte = self.control_change
        m = MidiMsg(self.set_status(byte), self.parameter, value)
        return m.msg

    def __repr__(self):
        return f"<{type(self).__name__}(CC {self.parameter}, Channel {self.channel})>"


class ProgramChange(MidiEvent):
    # kwargs: channel: int=channel
    def __call__(self, program) -> MidiMsg.msg:
        byte = self.program_change
        m = MidiMsg(self.set_status(byte), program)
        return m.msg

    def __repr__(self):
        return f"<{type(self).__name__}(Channel {self.channel})>"
