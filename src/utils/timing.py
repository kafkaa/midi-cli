from functools import partial
from collections.abc import Iterator
from typing import Any, Callable, Union

class EventClock(Iterator):
    """manage midi events

       cyclic generator with access to current position.
       executes a callback at the start of each cycle.

        EXAMPLE:
            bar = EventClock(96, next, cycle(range(1, 5)))
            bar(incoming-clock-msg)
    """
    _pos: int = -1

    def __init__(self, size: int, callback: Callable, *args: Any, **kwargs: Any):
        self._loop = size
        self._callback = partial(callback, *args, **kwargs)

    def __repr__(self):
        return f"<class {type(self).__name__}(size={self._size}, position={self._pos})>"

    def __iter__(self):
        return self

    def __next__(self):
        self._pos += 1
        value = self._pos % self._loop
        if value == 0:
            self._callback()
        return value

    def __call__(self, event: Union[int, float, str]):
        self.__next__()

    @property
    def position(self):
        return self._pos % self._loop

    @property
    def reset(self):
        self._pos = -1
