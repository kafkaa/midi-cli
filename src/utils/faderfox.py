from typing import NamedTuple

from .fileops import DBSE
from dbs.sql.sqldb import collect_tables

class Preset(NamedTuple):
    name: str
    parameters: dict

PRESETS = collect_tables(DBSE)

def fmsg(presets: list, chan: str, cc: str, val: str) -> str:
    def get_preset(channel: str) -> Preset | None:
        try:
            preset = presets[int(channel)-176]
            name = next(iter(preset))
            return Preset(name, dict(preset[name]))

        except IndexError:
            return None

    preset = get_preset(chan)
    if preset:
        n = preset.name
        p = preset.parameters.get(int(cc), preset.parameters[1])
        return f"{n[:16].ljust(16)}{p[:12].ljust(12)} {val}\n"
    return 'Unassigned'
