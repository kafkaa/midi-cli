import asyncio
from time import time
from functools import partial

from src.utils.status import CLOCK
from src.utils.helpers import Singleton
from src.utils.helpers import attributes


@attributes(CLOCK)
class MidiStreamer(metaclass=Singleton):
    # listen to and process midi data stream sent from rtmidi over specified midi port
    def __init__(self, port, policy=None, clock=True, sysex=True, callback=None, *args, **kwargs):
        """arguments:
            port        :   rtmidi.MidiIn()
            policy      :   sets asyncio event loop policy
            clock       :   filter midi clock messages, default is to filter the clock message
            sysex       :   filter system exclusive  messages, default is to filter the sysex messages
            callback    :   function to process the incoming midi stream
                            this function must accommodate the arguments: (msg, delta, timestamp)
            args, kwargs:   extra arguemnts for the callback function if necessary
        """
        self.port = port
        self.name = None
        self.sysex = sysex
        self.clock = clock
        self.policy = policy
        self._callback = partial(callback, *args, **kwargs)

    def __repr__(self):
        return f'{type(self).__name__}(port={self.name})'

    def open(self, port: int):
        self.port.open_port(port)
        self.name = self.port.get_port_name(port)
        print(f'Listening on input: {self.name}', flush=True)

    def close(self):
        self.port.close_port()
        print(f'Closed Input: {self.name}', flush=True)
        self.name = None

    def is_open(self) -> bool:
        return self.port.is_port_open()

    def enqueue(self, midipacket, data=None):
        # syncronization callback, stream feeder
        msg, delta = midipacket
        queue_packet = (msg, delta, time())
        try:
            self.loop.call_soon_threadsafe(self.midistream.put_nowait, queue_packet)

        except BaseException as failure:
            print(f"callback exc: {type(failure)} {failure}", flush=True)

    async def dequeue(self):
        while True:
            msg, delta, timestamp = await self.midistream.get()
            self._callback(msg, delta, timestamp)

    async def stream_data(self):
        # main coroutine
        self.port.ignore_types(timing=self.clock, sysex=self.sysex)
        self.port.set_callback(self.enqueue)
        self.loop = asyncio.get_event_loop()
        self.midistream = asyncio.Queue(maxsize=256)

        try:
            await self.dequeue()

        except asyncio.CancelledError:
            self.port.cancel_callback()

    def listen(self):
        try:
            asyncio.set_event_loop_policy(self.policy)
            asyncio.run(self.stream_data())

        except KeyboardInterrupt:
            print('Closing Event Loop', flush=True)
            self.close()
