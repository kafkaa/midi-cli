# functions for manipulating the midi input stream
import sys
from typing import Callable
from system.peripherals import lcd

def display() -> None:
    try:
        for line in sys.stdin:
            if line.startswith('['):
                msg, delta, timestamp = line.split('-')
                m, d, t = f"MIDI: {msg}", f"DELTA: {delta}", f"TIMESTAMP: {timestamp}"
                sys.stdout.write("%-25s %-30s %-20s" % (m, d, t))
            else:
                sys.stdout.write(line)
            sys.stdout.flush()

    except KeyboardInterrupt:
        sys.stdout.write(sys.stdin.read())
        sys.exit()


def lcd_display(device: str, splash=None, data: list=None, format: Callable=None) -> None:
    def change(cc: int, stored: int) -> tuple[bool, int]:
        if cc != stored:
            stored = cc
            return True, stored
        return False, stored
    
    current = None                             
    s = lcd.connect(device)
    if splash:
        s.write(bytes(splash[:16], 'utf-8'))

    try:
        for line in sys.stdin:
            if line.startswith('['):
                msg, *_ = line.split('-')
                msg = msg.strip('[]').replace(',', '').split()
                changed, current = change(msg[1], current)
                if changed:
                    lcd.update(s, format(data, *msg))
                else:
                    lcd.set_cursor(s, 14, 2)
                    s.write(bytes(msg[2].ljust(3), 'utf-8'))
            else:
                sys.stdout.write(line)
            sys.stdout.flush()

    except KeyboardInterrupt:
        lcd.disconnect(s)
        sys.stdout.write(sys.stdin.read())
        sys.exit()


def lcd_test(device: str, data: list=None, format: Callable=None) -> None:
    # writes formatted message to stdout for troublshooting
    try:
        for line in sys.stdin:
            if line.startswith('['):
                msg, _, _ = line.split('-')
                msg = msg.strip('[]').replace(',', '').split()
                sys.stdout.write(format(data, *msg))
            else:
                sys.stdout.write(line)
            sys.stdout.flush()

    except KeyboardInterrupt:
        sys.stdout.write(sys.stdin.read())
        sys.exit()

