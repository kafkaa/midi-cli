from time import sleep
from rtmidi import midiutil

class SyxFileError(Exception):
    pass

def send_system_exclusive(port: int, filename: str) -> None:
    # sends the specified sysex file to the specified midi port
    with open(filename, 'rb') as sysex:
        syx = sysex.read()

        if syx.startswith(b'\xf0') and syx.endswith(b'\xf7'):
            mout, name = midiutil.open_midioutput(port)
            print(f'opening port: {name}')
            with mout:
                print('sending data...')
                mout.send_message(syx)

            sleep(0.5)
            del mout
            print('data sent successfully')

        else:
            raise SyxFileError('Not A System Exclusive Midi File')

