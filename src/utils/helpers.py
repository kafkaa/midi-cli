import os
import sys
import json
import contextlib


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


def attributes(attributes: dict):
    # decorator: set class attributes using a dictionary
    def cls_attrs(cls):
        for attribute, value in attributes.items():
            setattr(cls, attribute, value)
        return cls
    return cls_attrs


def devnull(func):
    # decorator to redirect stderr to /dev/null
    def nullified(*args, **kwargs):
        with open(os.devnull, 'w') as null:
            with contextlib.redirect_stderr(null):
                func(*args, **kwargs)
    return nullified


def hex_to_dec(filename: str) -> list:
    # returns a list of decimals representing the hex midi bytes of the file
    with open(filename, 'rb') as syx:
        return [i for i in syx.read()]


HEXCODES = ['a', 'b', 'c', 'd', 'e', 'f']
CHMAP = dict(zip(range(1, 17), list(range(10)) + HEXCODES))
