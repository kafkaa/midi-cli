MODES = {
    "omni_on": [125, 0],
    "omni_off": [124, 0],
    "poly_mode": [127, 0],
    "mono_mode": [126, None],
    "all_notes_off": [123, 0],
    "reset_controllers": [121, 0]
}

VOICE = {
    "note_on": "0x9",
    "note_off": "0x8",
    "polypressure": "0xA",
    "control_change": "0xB",
    "program_change": "0xC",
    "channelpressure": "0xD",
    "pitchbend": "0xE"
}

CLOCK = {
    "clock": 248,
    "start": 250,
    "continue": 251,
    "stop": 252,
    "activesensing": "0xFE",
    "systemreset": "0xFF"
}
