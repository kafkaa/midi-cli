"""midi 2.0 [2022-07-01]

A simple midi diagnostic tool

USAGE:
    midi [command] [--options]

COMMANDS:
    ports   : access available midi ports
    note    : send a midi note event
    sysex   : send system exclusive message
    stream  : capture midi input stream
    control : send a midi control change event
    program : send a midi program change event
    through : redirect input stream

DOCS:
    midi command help

MIDI specification: @https://www.midi.org/specifications
"""
import sys
import importlib

this = sys.modules[__name__]

if __name__ == '__main__':

    try:
        command = sys.argv[1]
        module = importlib.import_module(f'src.cmds.{command}')
        command = getattr(module, command)
        command = command(sys.argv[1:])
        command.execute()

    except (IndexError, ModuleNotFoundError) as error:
        print(error)
        print(this.__doc__)
