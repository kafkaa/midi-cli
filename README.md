# MIDI Diagnostic and Stream Manipulation Tool

A simple command line utility for reading, formatting and sending raw midi messages.

## Overview

Midi is a command line diagnostic utility used to format and send 
midi messages. 

It sends Note On/Off, Control Change and Program Change messages. It can be used
deploy system-exclusive messages/files to midi devices. It can now also display 
a device's midi input stream in realtime, which can then be easily manipulated
and passed to other midi or serial devices such as LCDs.

## Requirements

+ [python](https://www.python.org)
+ [pipenv](https://pipenv.pypa.io/en/latest/)
+ [rtmidi](https://spotlightkid.github.io/python-rtmidi/)

## Getting Started

    git clone https://gitlab.com/kafkaa/midi-tool
    cd midi-tool
    pipenv install
    echo -e "#\!$(pwd)/.venv/bin/python3\n$(cat main.py)" > main.py
    chmod u+x main.py 
    cd /usr/local/bin
    ln -s /full/path/to/main.py midi

## Basic Usage

### Display Available MIDI Ports
    midi ports available

### Set Active MIDI Ports
    midi ports input 0
    midi ports output 0

### Send MIDI Note Event
    midi note -pitch 60 -vel 100 -dur 500 -chan 10

### Send MIDI Control Event
    midi control -param 60 -val 100 -chan 16

### Send MIDI Program Event
    midi program -val 100 -chan 16

### Send System Exclusive Message
    midi sysex send -f /path/to/file.syx

### Stream Raw Input to the Stdout
    midi stream input -cs

### Send Input Stream Through Another Process
    midi stream input | midi through $subcommand --options

#### Input Stream through FaderFox LCD Example
    midi stream input | midi through lcd /dev/ttyACM0 -s Faderfox PC4

![](https://gitlab.com/kafkaa/midi-tool/-/raw/novation-launch/pc4-lcd.mp4)

## Reference

+ [MIDI SPECIFICATION](https://www.midi.org/specifications)
+ [SERIAL LCD DEVICE](https://www.adafruit.com/product/784)
+ [NOVATION LAUNCHKEY](https://novationmusic.com/en/keys/launchkey)
+ [FADERFOX PC4 CONTROLLER](http://faderfox.de/pc4.html)

## License

+ [Apache](http://opensource.org/licenses/Apache-2.0)
